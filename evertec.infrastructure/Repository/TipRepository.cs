﻿namespace evertec.infrastructure
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using evertec.common;

    public class TipRepository : DataBaseService<Tip>, ITipRepository
    {
        public async Task<Tip> AddTip(Tip model)
        {
            return await Add(model);
        }

        public async Task<bool> DeleteTip(int id)
        {
            return await Delete(id);
        }

        public async Task<List<Tip>> GetAllTips()
        {
            return await GetAll();
        }

        public async Task<Tip> GetTipById(int id)
        {
            return await GetById(id);
        }

        public async Task<Tip> UpdateTip(Tip model)
        {
            return await Update(model);
        }
    }
}