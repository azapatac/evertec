﻿namespace evertec.infrastructure
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using evertec.common;
    using Microsoft.EntityFrameworkCore;

    public class DataBaseService<T> : IDataBaseService<T> where T : class
    {
        private DataBaseContext _dataBase;

        public DataBaseService()
        {
            _dataBase = new DataBaseContext(DataBaseHelper.DataBaseName);
        }

        public async Task<T> Add(T model)
        {
            await _dataBase.Set<T>().AddAsync(model);
            await _dataBase.SaveChangesAsync();
            return model;
        }

        public async Task<bool> Delete(int id)
        {
            var entity = await GetById(id);
            _dataBase.Set<T>().Remove(entity);
            await _dataBase.SaveChangesAsync();
            return true;
        }

        public async Task<List<T>> GetAll()
        {
            return await _dataBase.Set<T>().ToListAsync();
        }

        public async Task<T> GetById(int id)
        {
            return await _dataBase.Set<T>().FindAsync(id);
        }

        public async Task<T> Update(T model)
        {
            _dataBase.Set<T>().Update(model);
            
            await _dataBase.SaveChangesAsync();
            return model;
        }
    }
}