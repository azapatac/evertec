﻿namespace evertec.infrastructure
{
    using evertec.common;
    using Microsoft.EntityFrameworkCore;

    public class DataBaseContext : DbContext
    {
        private string _path;

        DbSet<Tip> Tips { get; set; }

        public DataBaseContext(string path)
        {
            _path = path;
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite($"Filename={_path}");
        }
    }
}