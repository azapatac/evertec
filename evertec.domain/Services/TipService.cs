﻿namespace evertec.domain
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using evertec.common;

    public class TipService : ITipService
    {
        private readonly ITipRepository _tipRepository;

        public TipService(ITipRepository tipRepository)
        {
            _tipRepository = tipRepository;
        }

        public async Task<Tip> AddTip(Tip model)
        {
            return await _tipRepository.AddTip(model);
        }

        public async Task<bool> DeleteTip(int id)
        {
            return await _tipRepository.DeleteTip(id);
        }

        public async Task<List<Tip>> GetAllTips()
        {
            return await _tipRepository.GetAllTips();
        }

        public async Task<Tip> GetTipById(int id)
        {
            return await _tipRepository.GetTipById(id);
        }

        public async Task<Tip> UpdateTip(Tip model)
        {
            return await _tipRepository.UpdateTip(model);
        }
    }
}