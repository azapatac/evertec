﻿namespace evertec.app
{
    using Prism;
    using Prism.Ioc;

    public partial class App
    {
        public App() : this(null) { }

        public App(IPlatformInitializer initializer) : base(initializer) { }

        protected override async void OnInitialized()
        {
            InitializeComponent();
            var result = await NavigationService.NavigateAsync("LoaderPage");
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            DependencyContainer.Register(containerRegistry);
            MapperContainer.Register(containerRegistry);
            PagesContainer.Register(containerRegistry);
        }

        protected override void OnStart() { }

        protected override void OnSleep() { }        

        protected override void OnResume() { }
    }
}