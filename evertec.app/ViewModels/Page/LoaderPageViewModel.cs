﻿namespace evertec.app
{
    using System.Threading.Tasks;
    using evertec.common;
    using Prism.Navigation;

    public class LoaderPageViewModel : BaseViewModel
    {
        public LoaderPageViewModel(INavigationService navigation, IDataBase dataBase) : base(navigation)
        {
            DataBaseHelper.DataBaseName = dataBase.Path;
        }

        public override async void Initialize(INavigationParameters parameters)
        {
            await Task.Delay(100);
            await _navigation.NavigateAsync(NavigationRoutes.MAIN);
            base.Initialize(parameters);
        }
    }
}