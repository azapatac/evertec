﻿namespace evertec.app
{
    using System;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using AutoMapper;
    using evertec.common;
    using Prism.Commands;
    using Prism.Navigation;

    public class TipPageViewModel : BaseViewModel
    {
        private readonly IMapper _mapper;
        private readonly ITipService _tipService;

        public TipViewModel Tip { get; set; }

        public ICommand SaveTipCommand => new DelegateCommand(ValidateTip);

        public TipPageViewModel(INavigationService navigation, ITipService tipService, IMapper mapper) : base(navigation)
        {
            Tip = new TipViewModel();

            _mapper = mapper;
            _tipService = tipService;
        }

        public override async void Initialize(INavigationParameters parameters)
        {
            if (parameters.ContainsKey("Id"))
            {
                var id = parameters.GetValue<int>("Id");
                await LoadTip(id);
            }

            base.Initialize(parameters);    
        }

        private async Task LoadTip(int id)
        {
            var tip = await _tipService.GetTipById(id);
            Tip = _mapper.Map<TipViewModel>(tip);            
        }

        private async void ValidateTip()
        {
            if (Tip.Id == 0)
            {
                await SaveTip();
                return;
            }
            
            await UpdateTip();
            await App.Current.MainPage.DisplayAlert("Tip", "Actualizado con exito !!", "Aceptar");

            var parameters = new NavigationParameters();
            parameters.Add("Updated", true);

            await _navigation.GoBackAsync(parameters);
            return;
            
        }

        private async Task SaveTip()
        {
            var tip = new Tip
            {
                CreationDate = Tip.CreationDate,
                Description = Tip.Description,
                Title = Tip.Title,
                UpdateDate = DateTime.Now
            };

            await _tipService.AddTip(tip);

            var parameters = new NavigationParameters();
            parameters.Add("Added", true);

            await _navigation.GoBackAsync(parameters);

        }

        private async Task UpdateTip()
        {
            var tip = await _tipService.GetTipById(Tip.Id);
            tip.CreationDate = Tip.CreationDate;
            tip.Description = Tip.Description;
            tip.Title = Tip.Title;
            tip.UpdateDate = DateTime.Now;
            await _tipService.UpdateTip(tip);
        }
    }
}