﻿namespace evertec.app
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using AutoMapper;
    using evertec.common;
    using Prism.Commands;
    using Prism.Navigation;

    public class MainPageViewModel : BaseViewModel
    {
        private readonly ITipService _tipService;
        private readonly IMapper _mapper;

        public ICollection<TipViewModel> Tips { get; set; }

        public ICommand DeleteTipCommand => new DelegateCommand<TipViewModel>((tip) => DeleteTip(tip));
        public ICommand EditTipCommand => new DelegateCommand<TipViewModel>((tip) => EditTip(tip));
        public ICommand NewTipCommand => new DelegateCommand(NewTip);

        public MainPageViewModel(INavigationService navigation, ITipService tipService, IMapper mapper) : base(navigation)
        {
            Tips = new ObservableCollection<TipViewModel>();

            _mapper = mapper;
            _tipService = tipService;
        }

        public override async void Initialize(INavigationParameters parameters)
        {
            await LoadTips();
            base.Initialize(parameters);
        }

        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            if (parameters.ContainsKey("Updated") && parameters.GetValue<bool>("Updated") ||
                parameters.ContainsKey("Added") && parameters.GetValue<bool>("Added"))

            {
                await LoadTips();
            }

            base.OnNavigatedTo(parameters);
        }

        private async Task LoadTips()
        {
            var tips = await _tipService.GetAllTips();
            var orderedTips = tips.OrderBy(x => x.CreationDate);
            Tips = _mapper.Map<ObservableCollection<TipViewModel>>(orderedTips);
        }

        private async void DeleteTip(TipViewModel tip)
        {
            var delete = await App.Current.MainPage.DisplayAlert("Tip", "¿Borrar el tip??", "Si", "No");
            if (!delete)
                return;

            await _tipService.DeleteTip(tip.Id);
            Tips.Remove(tip);
        }

        private async void EditTip(TipViewModel tip)
        {
            var parameters = new NavigationParameters();
            parameters.Add("Id", tip.Id);
            await _navigation.NavigateAsync(NavigationRoutes.TIP, parameters);
        }

        private async void NewTip()
        {
            await _navigation.NavigateAsync(NavigationRoutes.TIP);
        }
    }
}