﻿namespace evertec.app
{
    using System;

    public class TipViewModel : BaseViewModel
    {
        public int Id { get; set; }
        public DateTime CreationDate { get; set; }
        public string Description { get; set; }
        public string Title { get; set; }
        public DateTime UpdateDate { get; set; }

        public TipViewModel()
        {
            CreationDate = DateTime.Today;
        }
    }
}