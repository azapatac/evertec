﻿namespace evertec.app
{
    using Prism.Mvvm;
    using Prism.Navigation;

    public class BaseViewModel : BindableBase, IInitialize, INavigationAware
    {
        protected INavigationService _navigation { get; set; }

        public BaseViewModel(INavigationService navigation)
        {
            _navigation = navigation;
        }

        public BaseViewModel() { }

        #region IInitialize
        public virtual void Initialize(INavigationParameters parameters) { }
        #endregion

        #region INavigationAware
        public void OnNavigatedFrom(INavigationParameters parameters) { }
        public virtual void OnNavigatedTo(INavigationParameters parameters) { }
        #endregion
    }
}