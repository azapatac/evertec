﻿namespace evertec.app
{
    public static class IconFont
    {
        public const string AngleDown = "\uf107";
        public const string AngleUp = "\uf106";
        public const string CalendarAlt = "\uf073";
        public const string Eye = "\uf06e";
        public const string PlusCircle = "\uf055";
        public const string Save = "\uf0c7";
        public const string Trash = "\uf1f8";
    }
}
