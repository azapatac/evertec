﻿namespace evertec.app
{
    using AutoMapper;
    using evertec.common;

    public class TipProfile : Profile
    {
        public TipProfile()
        {
            CreateMap<Tip, TipViewModel>().ReverseMap();
        }
    }
}