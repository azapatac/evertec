﻿namespace evertec.app
{
    using AutoMapper;
    using Prism.Ioc;

    public static class MapperContainer
    {
        public static void Register(IContainerRegistry containerRegistry)
        {
            MapperConfiguration mapperConfiguration = new MapperConfiguration((config) =>
            {
                config.AddProfile(new TipProfile());
            });

            IMapper mapper = mapperConfiguration.CreateMapper();
            containerRegistry.RegisterInstance(typeof(IMapper), mapper);
        }
    }
}
