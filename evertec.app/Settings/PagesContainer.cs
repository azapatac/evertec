﻿namespace evertec.app
{
    using Prism.Ioc;
    using Xamarin.Forms;

    public static class PagesContainer
    {
        public static void Register(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<NavigationPage>();

            containerRegistry.RegisterForNavigation<LoaderPage, LoaderPageViewModel>();
            containerRegistry.RegisterForNavigation<MainPage, MainPageViewModel>();
            containerRegistry.RegisterForNavigation<TipPage, TipPageViewModel>();
        }
    }
}