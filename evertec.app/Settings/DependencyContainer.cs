﻿namespace evertec.app
{
    using evertec.common;
    using evertec.domain;
    using evertec.infrastructure;
    using Prism.Ioc;

    public static class DependencyContainer
    {
        public static void Register(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterSingleton<ITipRepository, TipRepository>();

            containerRegistry.RegisterSingleton<ITipService, TipService>();
        }
    }
}