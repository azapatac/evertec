﻿namespace evertec.common
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Tip
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public DateTime CreationDate { get; set; }
        public string Description { get; set; }
        public string Title { get; set; }
        public DateTime UpdateDate { get; set; }
    }
}