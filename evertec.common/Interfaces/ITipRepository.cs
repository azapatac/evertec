﻿namespace evertec.common
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface ITipRepository
    {
        Task<Tip> AddTip(Tip model);
        Task<bool> DeleteTip(int id);
        Task<List<Tip>> GetAllTips();
        Task<Tip> GetTipById(int id);
        Task<Tip> UpdateTip(Tip model);
    }
}