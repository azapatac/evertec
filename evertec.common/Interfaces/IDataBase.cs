﻿namespace evertec.common
{
    public interface IDataBase
    {
        public string Path { get; }
    }
}