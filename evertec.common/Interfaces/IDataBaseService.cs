﻿namespace evertec.common
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IDataBaseService<T> where T : class
    {
        Task<T> Add(T model);
        Task<bool> Delete(int id);
        Task<List<T>> GetAll();
        Task<T> GetById(int id);
        Task<T> Update(T model);
    }
}