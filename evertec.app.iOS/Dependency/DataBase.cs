﻿namespace evertec.app.iOS
{
    using System;
    using evertec.common;
    using evertec.common.Constants;

    public class DataBase : IDataBase
    {
        public string Path => GetDatabasePath();

        private string GetDatabasePath()
        {
            return System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "..", "Library", Settings.DATABASE_NAME);
        }
    }
}