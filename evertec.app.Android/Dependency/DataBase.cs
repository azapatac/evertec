﻿namespace evertec.app.Droid
{
    using System;
    using evertec.common;
    using evertec.common.Constants;

    public class DataBase : IDataBase
    {
        public string Path => GetDatabasePath();

        private string GetDatabasePath()
        {
            return System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), Settings.DATABASE_NAME);
        }
    }
}